﻿using UnityEngine;
using System.Collections;

public class CubeController : MonoBehaviour {

	public float speed = 50.0f;
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.left, speed * Time.deltaTime);
	}
}
